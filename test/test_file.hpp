#include <QString>
#include <QtTest>

#include <file.hpp>
#include <QFile>
#include <QString>
#include <QTextStream>


class TestFile : public QObject
{
    Q_OBJECT
public:
    TestFile();

private:
    const QString filename_read = "read.test";
    const QString filename_write = "write.test";
    const QString content_to_read = "Hello World :)";

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void test_read();
    void test_write();
};
