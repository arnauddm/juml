#include <QtTest>
#include <QObject>

#include <file.hpp>
#include <class.hpp>

class TestClass : public QObject
{
    Q_OBJECT
public:
    TestClass();

private:
    Class * _testClass;


private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void simpleClass();
    void umlLink();
    void classDeclaration();
    void complexClass();
};
