#include "test_file_library.hpp"
#include <file.hpp>

TestFileLibrary::TestFileLibrary()
{

}

void TestFileLibrary::initTestCase() {
    QStringList class1;
    class1.append("import java.org.test;");
    class1.append("class classA extends ext1 {");
    class1.append("}");

    QStringList class2;
    class2.append("import java.org.test;");
    class2.append("class classB extends ext2 {");
    class2.append("}");

    QStringList cmd;
    cmd.append("cd /tmp");
    cmd.append("mkdir testJuml");
    cmd.append("cd testJuml");
    cmd.append("echo \"" + class1.join("\n") + "\" >> classA.java");
    cmd.append("mkdir myDir");
    cmd.append("cd myDir");
    cmd.append("echo \"" + class2.join("\n") + "\" >> classB.java");

    system(cmd.join(";").toStdString().c_str());
}

void TestFileLibrary::cleanupTestCase() {
    const QString cmd = "rm -rf /tmp/testJuml;";
    system(cmd.toStdString().c_str());
}

void TestFileLibrary::checkFiles() {
    FileLibrary lib;
    QCOMPARE(true, lib.scan("/tmp/testJuml"));
    QCOMPARE(2, lib.count());
    QCOMPARE(true, lib.get(0)->fileName().contains("classA.java"));
    QCOMPARE(true, lib.get(1)->fileName().contains("classB.java"));
}

void TestFileLibrary::parseDirectoryFile() {
    FileLibrary lib;
    QCOMPARE(true, lib.scan("/tmp/testJuml"));

    QVector<Class *> classes = lib.getClasses();
    QCOMPARE(2, classes.size());

    QVector<QString> classesName;
    for(int i = 0; i < classes.size(); i++)
        classesName.append(classes.at(i)->getClassName());

    qDebug() << classesName;

    QCOMPARE(true, classesName.contains("classA"));
    QCOMPARE(true, classesName.contains("classB"));
    QCOMPARE(false, classesName.contains("classC"));
}
