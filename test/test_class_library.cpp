#include "test_class_library.hpp"

#include <class.hpp>

TestClassLibrary::TestClassLibrary()
{
}

void TestClassLibrary::initTestCase() {
    _classLib = new ClassLibrary();
    Class * class1 = new Class();
    class1->setClassName("class1");
    _classLib->add(class1);
}

void TestClassLibrary::cleanupTestCase() {
    if(_classLib)
        delete _classLib;
}

void TestClassLibrary::testSingleClass() {
    QCOMPARE(1, _classLib->count());
    QCOMPARE("class1", _classLib->get(0)->getClassName());
}

void TestClassLibrary::testAddNewClass() {
    Class * class2 = new Class();
    class2->setClassName("class2");

    QCOMPARE(true, _classLib->add(class2));
    QCOMPARE(2, _classLib->count());
}

void TestClassLibrary::testExistingClass() {
    Class * class3 = new Class();
    class3->setClassName("class1");

    QCOMPARE(false, _classLib->add(class3));
    QCOMPARE(2, _classLib->count());
}
