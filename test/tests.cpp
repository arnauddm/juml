#include <QTest>

#include <test_file.hpp>
#include <test_file_library.hpp>
#include <test_class.hpp>
#include <test_class_library.hpp>
#include <test_complete.hpp>

int main(int argc, char * argv[]) {
    QObject *classes[] = {
        new TestFile(),
        new TestFileLibrary(),
        new TestClass(),
        new TestClassLibrary(),
        new TestComplete()
    };

    qDebug() << "Total tests : " + sizeof(classes) / (2*sizeof(int));

    for(int i = 0; i < sizeof(classes) / (2*sizeof(int)); i++) {
        if(QTest::qExec(classes[i], argc, argv) != 0)
            return -1;
    }

    qDebug() << "ALL TESTS ARE SUCCESSFULLY PASSED";
    return 0;
}
