#include <file_library.hpp>
#include <QtTest>

class TestFileLibrary : public QObject
{
    Q_OBJECT
public:
    TestFileLibrary();

private:

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void checkFiles();
    void parseDirectoryFile();
};
