#include <test_file.hpp>

TestFile::TestFile()
{
}

void TestFile::initTestCase()
{
    const QString cmd = "echo \"" + content_to_read + "\" >> " + filename_read;
    system(cmd.toStdString().c_str());
}

void TestFile::cleanupTestCase()
{
    const QString cmd_read = "rm " + filename_read;
    system(cmd_read.toStdString().c_str());

    const QString cmd_write = "rm " + filename_write;
    system(cmd_write.toStdString().c_str());
}

void TestFile::test_read()
{
    File file(filename_read);
    QString content = file.readContent();
    QCOMPARE(content_to_read + "\n", content);
}

void TestFile::test_write() {
    File file(filename_write);
    file.write(content_to_read);

    file.setFileName(filename_read);
    QCOMPARE(content_to_read + "\n", file.readContent());
}
