#include "test_complete.hpp"

#include <file.hpp>
#include <file_library.hpp>
#include <class_library.hpp>
#include <class.hpp>

TestComplete::TestComplete()
{
}

void TestComplete::initTestCase() {
    QStringList class1;
    class1.append("import java.org.test;");
    class1.append("class classA extends ext1 {");
    class1.append("}");

    QStringList class2;
    class2.append("import java.org.test;");
    class2.append("class classB extends ext2 {");
    class2.append("}");

    QStringList cmd;
    cmd.append("cd /tmp");
    cmd.append("mkdir testJuml");
    cmd.append("cd testJuml");
    cmd.append("echo \"" + class1.join("\n") + "\" >> classA.java");
    cmd.append("mkdir myDir");
    cmd.append("cd myDir");
    cmd.append("echo \"" + class2.join("\n") + "\" >> classB.java");

    system(cmd.join(";").toStdString().c_str());
}

void TestComplete::cleanupTestCase() {
    QString cmd = "rm -rf /tmp/testJuml;";
    system(cmd.toStdString().c_str());
}

void TestComplete::verify() {
    FileLibrary lib;

    QCOMPARE(true, lib.scan("/tmp/testJuml"));

    QVector<Class *> classes = lib.getClasses();
    QCOMPARE(2, classes.size());

    for(int i = 0; i < classes.size(); i++) {
        Class * c = classes.at(i);
        if(c->getClassName() == "classA") {
            QCOMPARE(1, c->getExtends().size());
            QCOMPARE("ext1", c->getExtends().at(0));
        } else if(c->getClassName() == "classB") {
            QCOMPARE(1, c->getExtends().size());
            QCOMPARE("ext2", c->getExtends().at(0));
        }
    }

    QString uml = ClassLibrary::toUml(classes);
    QCOMPARE(true, uml.contains("class classA"));
    QCOMPARE(true, uml.contains("class classB"));
    QCOMPARE(true, uml.contains("classA --> ext1"));
    QCOMPARE(true, uml.contains("classB --> ext2"));
}
