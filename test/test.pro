#-------------------------------------------------
#
# Project created by QtCreator 2020-03-07T16:53:07
#
#-------------------------------------------------

QT       += core testlib
QT       -= gui

TARGET = exe_tests
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Test sources
SOURCES += \
    tests.cpp \
    test_file.cpp \
    test_file_library.cpp \
    test_class.cpp \
    test_class_library.cpp \
    test_complete.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

INCLUDEPATH += ../src
DEPENDPATH += ../src

HEADERS += \
        test_file.hpp \
        test_file_library.hpp \
        test_class.hpp \
    test_class_library.hpp \
    test_complete.hpp

SOURCES += ../src/file_library.cpp
HEADERS += ../src/file_library.hpp

SOURCES += ../src/file.cpp
HEADERS += ../src/file.hpp

SOURCES += ../src/class.cpp
HEADERS += ../src/class.hpp

SOURCES += ../src/class_library.cpp
HEADERS += ../src/class_library.hpp
