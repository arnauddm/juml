#ifndef TEST_COMPLETE_HPP
#define TEST_COMPLETE_HPP

#include <QtTest>
#include <QObject>

class TestComplete : public QObject
{
    Q_OBJECT
public:
    TestComplete();

private Q_SLOTS:
    void initTestCase();
    void verify();
    void cleanupTestCase();
};

#endif // TEST_COMPLETE_HPP
