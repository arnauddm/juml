#include "test_class.hpp"
#include <file.hpp>
#include <class.hpp>

TestClass::TestClass()
{

}

void TestClass::initTestCase() {
    QString content = "import java.org.test;\n"
                      "class MyClass extends MyExtension implements MyImplementation {\n"
                      "}\n";
    QString cmd = "echo \"" + content + "\" >> /tmp/test_class.java";
    system(cmd.toStdString().c_str());

    _testClass = new Class();
    _testClass->setClassName("myClass");
    _testClass->addExtend("myExtends");
    _testClass->addImplement("imp1");
    _testClass->addImplement("imp2");
}

void TestClass::simpleClass() {
    File file("/tmp/test_class.java");
    Class classTest(&file);
    QCOMPARE(true, classTest.parse());

    QCOMPARE("MyClass", classTest.getClassName());

    QCOMPARE(1, classTest.getExtends().size());
    QCOMPARE("MyExtension", classTest.getExtends().at(0));

    QCOMPARE(1, classTest.getImplements().size());
    QCOMPARE("MyImplementation", classTest.getImplements().at(0));

    cleanupTestCase();
}

void TestClass::complexClass() {
    QString content = "import java.org.test;\n"
                      "class MyClass extends MyExtension implements MyImplementation, myImplementation {\n"
                      "}\n";
    QString cmd = "echo \"" + content + "\" >> /tmp/test_class.java";
    system(cmd.toStdString().c_str());

    File file("/tmp/test_class.java");
    Class classTest(&file);
    QCOMPARE(true, classTest.parse());

    QCOMPARE("MyClass", classTest.getClassName());

    QCOMPARE(1, classTest.getExtends().size());
    QCOMPARE("MyExtension", classTest.getExtends().at(0));

    QCOMPARE(2, classTest.getImplements().size());
    QCOMPARE("MyImplementation", classTest.getImplements().at(0));
    QCOMPARE("myImplementation", classTest.getImplements().at(1));
}

void TestClass::umlLink() {

    QList<QString> final;
    final.append("myClass --> myExtends");
    final.append("myClass --> imp1");
    final.append("myClass --> imp2");

    QCOMPARE(final.join("\n"), _testClass->getUmlRelations());
}

void TestClass::classDeclaration() {
    QList<QString> final;
    final.append("class myClass {");
    final.append("}");

    QCOMPARE(final.join("\n"), _testClass->getClassDeclaration());

}


void TestClass::cleanupTestCase() {
    QString cmd = "rm /tmp/test_class.java";
    system(cmd.toStdString().c_str());
}
