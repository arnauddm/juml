#ifndef TEST_CLASS_LIBRARY_HPP
#define TEST_CLASS_LIBRARY_HPP

#include <QObject>
#include <QtTest>
#include <class_library.hpp>

class TestClassLibrary : public QObject
{
    Q_OBJECT
public:
    TestClassLibrary();

private:
    ClassLibrary * _classLib;

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testSingleClass();
    void testAddNewClass();
    void testExistingClass();
};

#endif // TEST_CLASS_LIBRARY_HPP
