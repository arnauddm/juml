#ifndef JUML_HPP
#define JUML_HPP

#include <QMainWindow>
#include <QString>
#include <QListWidget>
#include <QListWidgetItem>
#include <file_library.hpp>
#include <class_library.hpp>

#include "ui_juml.h"

class juml : public QMainWindow, Ui_juml
{
    Q_OBJECT

public:
    explicit juml(QWidget *parent = 0);
    ~juml();

private:
    FileLibrary fileLibrary;
    QString folderPath;

public slots:
    void selectFolder(void);
    void parse(void);
    void fileClicked(QListWidgetItem * item);
};

#endif // JUML_HPP
