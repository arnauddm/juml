#ifndef CLASS_LIBRARY_HPP
#define CLASS_LIBRARY_HPP

#include <class.hpp>

class ClassLibrary
{
public:
    ClassLibrary();
    ~ClassLibrary();

    bool add(Class * classToAdd);
    Class * get(int index);
    int count(void);

    static QString toUml(QVector<Class *> classes);

private:
    QVector<Class *> _classes;

    bool exist(Class * classToTest);
};

#endif // CLASS_LIBRARY_HPP
