/********************************************************************************
** Form generated from reading UI file 'juml.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JUML_H
#define UI_JUML_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_juml
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QPushButton *parse_pushButton;
    QPushButton *select_pushButton;
    QPlainTextEdit *output_textEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QGridLayout *gridLayout_2;
    QListWidget *fileList;

    void setupUi(QMainWindow *juml)
    {
        if (juml->objectName().isEmpty())
            juml->setObjectName(QString::fromUtf8("juml"));
        juml->resize(463, 440);
        centralWidget = new QWidget(juml);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        parse_pushButton = new QPushButton(centralWidget);
        parse_pushButton->setObjectName(QString::fromUtf8("parse_pushButton"));

        gridLayout->addWidget(parse_pushButton, 1, 1, 1, 1);

        select_pushButton = new QPushButton(centralWidget);
        select_pushButton->setObjectName(QString::fromUtf8("select_pushButton"));

        gridLayout->addWidget(select_pushButton, 1, 0, 1, 1);

        output_textEdit = new QPlainTextEdit(centralWidget);
        output_textEdit->setObjectName(QString::fromUtf8("output_textEdit"));
        output_textEdit->setReadOnly(true);

        gridLayout->addWidget(output_textEdit, 0, 0, 1, 2);

        juml->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(juml);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 463, 22));
        juml->setMenuBar(menuBar);
        mainToolBar = new QToolBar(juml);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        juml->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(juml);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        juml->setStatusBar(statusBar);
        dockWidget = new QDockWidget(juml);
        dockWidget->setObjectName(QString::fromUtf8("dockWidget"));
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        gridLayout_2 = new QGridLayout(dockWidgetContents);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        fileList = new QListWidget(dockWidgetContents);
        fileList->setObjectName(QString::fromUtf8("fileList"));

        gridLayout_2->addWidget(fileList, 0, 0, 1, 1);

        dockWidget->setWidget(dockWidgetContents);
        juml->addDockWidget(Qt::LeftDockWidgetArea, dockWidget);

        retranslateUi(juml);

        QMetaObject::connectSlotsByName(juml);
    } // setupUi

    void retranslateUi(QMainWindow *juml)
    {
        juml->setWindowTitle(QCoreApplication::translate("juml", "juml", nullptr));
        parse_pushButton->setText(QCoreApplication::translate("juml", "Parse", nullptr));
        select_pushButton->setText(QCoreApplication::translate("juml", "Select", nullptr));
    } // retranslateUi

};

namespace Ui {
    class juml: public Ui_juml {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JUML_H
