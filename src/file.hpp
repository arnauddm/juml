#ifndef FILE_HPP
#define FILE_HPP

#include <QFile>
#include <QString>
#include <QTextStream>

class File : public QFile
{
public:
    File(void);
    File(QString filepath);
    ~File(void);

    bool write(QString content);
    QString readContent(void);

    bool isDir(void);
    static bool isDir(QString filepath);

private:
};

#endif // FILE_HPP
