#include "class_library.hpp"

ClassLibrary::ClassLibrary(void) {
}

ClassLibrary::~ClassLibrary(void) {
    for(int i = 0; i < _classes.size(); i++)
        delete _classes.at(i);
}

bool ClassLibrary::add(Class * classToAdd) {
    if(exist(classToAdd))
        return false;

    _classes.append(classToAdd);
    return true;
}

Class * ClassLibrary::get(int index) {
    return index < 0 || index > count() - 1 ? nullptr : _classes.at(index);
}

int ClassLibrary::count(void) {
    return _classes.count();
}

bool ClassLibrary::exist(Class * classToTest) {
    for(int i = 0; i < _classes.size(); i++) {
        if(classToTest->getClassName() == get(i)->getClassName())
            return true;
    }
    return false;
}

QString ClassLibrary::toUml(QVector<Class *> classes) {
    QStringList classesDeclaration;
    QStringList classesAssociation;

    for(int i = 0; i < classes.size(); i++) {
        Class * c = classes.at(i);
        if(!c)
            continue;

        classesDeclaration.append(c->getClassDeclaration());
        classesAssociation.append(c->getUmlRelations());
    }

    return classesDeclaration.join("\n") + "\n" + classesAssociation.join("\n");

}
