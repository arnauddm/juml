#ifndef FILE_LIBRARY_HPP
#define FILE_LIBRARY_HPP

#include <file.hpp>
#include <class.hpp>
#include <QVector>

class FileLibrary
{
public:
    FileLibrary();
    ~FileLibrary();

    bool add(File * file);
    bool add(QVector<File *> files);
    File * get(int index);
    int count(void);
    bool scan(void);
    bool scan(QString dir);
    bool setDir(QString dir);
    QString getDir(void);

    QVector<Class *> getClasses();

private:
    QVector<File *> _files;
    QString _dir;

    bool exist(File * file);
    void recursiveScan(QString child);
};

#endif // FILE_LIBRARY_HPP
