#include "file.hpp"
#include <QFileInfo>

File::File(void) {
}

File::File(QString filepath) {
    File();
    setFileName(filepath);
}

File::~File(void) {
}

bool File::write(QString content) {
    if(fileName().isEmpty() || !open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream stream(this);
    stream << content;

    close();
    return true;
}

QString File::readContent(void) {
    if(fileName().isEmpty() || !open(QIODevice::ReadOnly | QIODevice::Text))
        return QString();
    QTextStream stream(this);
    QString content = stream.readAll();
    close();
    return content;
}

bool File::isDir(void) {
    QFileInfo info(*this);
    return info.isDir();
}

bool File::isDir(QString filepath) {
    QFileInfo info(filepath);
    return info.isDir();
}
