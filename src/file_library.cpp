#include "file_library.hpp"
#include <QDir>
#include <QStringList>
#include <QDebug>

FileLibrary::FileLibrary()
{
}

FileLibrary::~FileLibrary() {
    for(int i = 0; i < count(); i++) {
        delete get(i);
    }
}

int FileLibrary::count() {
    return _files.size();
}

File * FileLibrary::get(int index) {
    return index < 0 || index > count() - 1 ? nullptr : _files.at(index);
}

bool FileLibrary::add(File * file) {
    if(exist(file))
        return false;
    _files.append(file);
    return true;
}

bool FileLibrary::add(QVector<File *> files) {
    bool result = true;
    for(int i = 0; i < files.size(); i++)
        result &= add(files.at(i));
    return result;
}

bool FileLibrary::exist(File * file) {
    if(!file)
        return false;

    for(int i = 0; i < count(); i++) {
        File * current = get(i);
        if(current->fileName() == file->fileName())
            return true;
    }
    return false;
}

bool FileLibrary::setDir(QString dir) {
    _dir = dir;
    return true;
}

QString FileLibrary::getDir(void) {
    return _dir;
}

bool FileLibrary::scan(void) {
    if(!File(getDir()).exists())
        return false;
    recursiveScan(getDir());
    return true;
}

bool FileLibrary::scan(QString dir) {
    setDir(dir);
    return scan();
}

void FileLibrary::recursiveScan(QString child) {
    QDir dir(child);
    QStringList childrenFiles = dir.entryList();
    for(int i = 0; i < childrenFiles.size(); i++)
    {
        // Skipping "." and ".." shortcut in Linux
        if(childrenFiles.at(i) == "." || childrenFiles.at(i) == "..")
            continue;

        QString filePath = child + "/" + childrenFiles.at(i);

        if(File(filePath).isDir())
            recursiveScan(filePath);
        else
            add(new File(filePath));
    }
}

QVector<Class *> FileLibrary::getClasses() {
    QVector<Class *> classes;

    for(int i = 0; i < _files.size(); i++) {
        Class * c = new Class(_files.at(i));
        if(!c->parse()) {
            qDebug() << "Error while parsing " + _files.at(i)->fileName();
            delete c;
            continue;
        }
        classes.append(c);
    }

    return classes;
}
