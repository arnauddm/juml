#include "class.hpp"
#include <QDebug>

Class::Class(void) {
    _file = nullptr;
}

Class::Class(File * file) {
    Class();
    _file = file;
}

Class::~Class() {

}

bool Class::parse(void) {
    if(!_file)
        return false;

    QStringList lines = _file->readContent().split("\n");
    for(int i = 0; i < lines.size(); i++) {
        QString line = lines.at(i);
        if(!line.contains("class"))
            continue;

        QStringList keyWord = line.split(" ");
        QString currentKeyWorkd;
        for(int j = 0; j < keyWord.size(); j++) {
            QString word = keyWord.at(j);
            if(word == "class" || word == "extends" || word == "implements") {
                currentKeyWorkd = word;
                continue;
            }

            if(word.isEmpty() || word.contains("{") || word.contains("}"))
                continue;

            // If "implements classA,classB" but this behaviour works for "implements classA"
            QStringList subWord = word.split(",");
            for(int sub = 0; sub < subWord.size(); sub++) {

                QString keyWord = subWord.at(sub);
                keyWord.replace(" ", "");
                if(keyWord.isEmpty())
                    continue;

                keyWord = keyWord.replace(" ", "");
                if(currentKeyWorkd == "class")
                    _className = subWord.at(sub);
                else if(currentKeyWorkd == "extends")
                    _extends.append(subWord.at(sub));
                else if(currentKeyWorkd == "implements")
                    _implements.append(subWord.at(sub));
            }
        }
    }

    return true;
}


QString Class::getClassName(void) {
    return _className;
}

QVector<QString> Class::getExtends(void) {
    return _extends;
}

QVector<QString> Class::getImplements(void) {
    return _implements;
}

QString Class::getUmlRelations(void) {
    QList<QString> relations;
    for(int i = 0; i < _extends.size(); i++)
        relations.append(getClassName() + " --> " + _extends.at(i));
    for(int i = 0; i < _implements.size(); i++)
        relations.append(_className + " --> " + _implements.at(i));

    return relations.join("\n");
}

void Class::setClassName(QString className) {
    _className = className;
}

void Class::addExtend(QString extendClassName) {
    _extends.append(extendClassName);
}

void Class::addImplement(QString implementClassName) {
    _implements.append(implementClassName);
}

QString Class::getClassDeclaration(void) {
    QStringList classDeclaration;
    classDeclaration.append("class " + _className + " {");
    classDeclaration.append("}");
    return classDeclaration.join("\n");
}
