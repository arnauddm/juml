#include "juml.hpp"
#include <QFileDialog>

juml::juml(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);
    connect(select_pushButton, SIGNAL(clicked()), this, SLOT(selectFolder()));
    connect(parse_pushButton, SIGNAL(clicked()), this, SLOT(parse()));
    connect(fileList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(fileClicked(QListWidgetItem *)));
}

juml::~juml()
{
}

void juml::selectFolder(void) {
    folderPath = QFileDialog::getExistingDirectory();
    fileLibrary.scan(folderPath);

    fileList->clear();
    for(int i = 0; i < fileLibrary.count(); i++) {
        fileList->addItem(fileLibrary.get(i)->fileName());
    }
}

void juml::parse(void) {
     QString uml = ClassLibrary::toUml(fileLibrary.getClasses());
     this->output_textEdit->setPlainText(uml);
}

void juml::fileClicked(QListWidgetItem * item) {
    File file(item->text());
    output_textEdit->setPlainText(file.readContent());
}
