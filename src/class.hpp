#ifndef CLASS_HPP
#define CLASS_HPP

#include <file.hpp>
#include <QVector>
#include <QString>

class Class
{
public:
    Class(void);
    Class(File * file);
    ~Class(void);

    bool parse(void);

    QString getClassName(void);
    QVector<QString> getExtends(void);
    QVector<QString> getImplements(void);

    void setClassName(QString className);
    void addExtend(QString extendClassName);
    void addImplement(QString implementClassName);

    QString getClassDeclaration(void);
    QString getUmlRelations(void);

private:
    File * _file;
    QString _className;
    QVector<QString> _extends;
    QVector<QString> _implements;
};

#endif // CLASS_HPP
